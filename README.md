# Solactive Challenge App
## Endpoints
1) GET /ticks?ric={ric}&offset={offset}&limit={limit} <br/>
   Query params:
    - ric - RIC name
    - limit - max items (optional, default value = 10)
    - offset - skip items (optional, default value = 0) <br/>

   Response example: <br/>
```json
{
    "ticks": [
        {
            "timestamp": 1648490954412,
            "price": 1111.24,
            "closePrice": null,
            "currency": "EUR",
            "ric": "AAPL.OQ"
        },
        {
            "timestamp": 1648490954412,
            "price": null,
            "closePrice": 7.5,
            "currency": "EUR",
            "ric": "AAPL.OQ"
        }
    ]
}
```

## Settings
```properties
# Tick server port (TCP)
tick.server.port=8081
# Control server port (HTTP)
server.port=8080
# Tick csv files location
app.ticks.export.location=ticks
# Tick export csv thread pool size
app.ticks.export.pool.size=10
```

## CSV Export files
Filename pattern: {timestamp}-{global-ric-file-counter}.txt <br/>
Example content:
```text
1648490954412,,7.5,EUR,AAPL.OQ

```
