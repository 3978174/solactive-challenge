package com.solactive;

import com.solactive.tick.services.TickDecoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class TickServer {
  public static final String TICK_DELIMITER = "\n";
  private final TickDecoder tickDecoder;
  private Thread tickServerThread;

  @Value("${tick.server.port}")
  private Integer port;


  @PostConstruct
  public void startAsync() {
    tickServerThread = new Thread(this::startTickServer);
    tickServerThread.start();
  }

  @PreDestroy
  public void stopServer() {
    tickServerThread.interrupt();
  }


  @SneakyThrows
  public void startTickServer() {
    EventLoopGroup bossGroup = new NioEventLoopGroup();
    EventLoopGroup workerGroup = new NioEventLoopGroup();
    try {
      ServerBootstrap b = new ServerBootstrap();
      b.group(bossGroup, workerGroup)
          .channel(NioServerSocketChannel.class)
          .childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) {
              ch.pipeline().addLast(
                  new DelimiterBasedFrameDecoder(
                      512,
                      Unpooled.wrappedBuffer(TICK_DELIMITER.getBytes())
                  ),
                  tickDecoder
              );
            }
          }).option(ChannelOption.SO_BACKLOG, 128)
          .childOption(ChannelOption.SO_KEEPALIVE, true);

      ChannelFuture f = b.bind(port).sync();
      log.info("Tick server started on port {}", port);
      f.channel().closeFuture().await();
    } finally {
      log.info("Tick server shutdown");
      bossGroup.shutdownGracefully();
      workerGroup.shutdownGracefully();
    }
  }

}
