package com.solactive.tick.model;

import java.math.BigDecimal;
import java.text.MessageFormat;
import lombok.Data;

@Data
public class Tick {
  private final long timestamp;
  private final BigDecimal price;
  private final BigDecimal closePrice;
  private final TickCurrency currency;
  private final String ric;

  public Object[] toRecord() {
    return new Object[] {
        timestamp,
        price == null? null: price.toString(),
        closePrice == null? null: closePrice.toString(),
        currency.name(),
        ric
    };
  }

  public String toString() {
    return MessageFormat.format(
        "TIMESTAMP={0}|PRICE={1}|CLOSE_PRICE={2}|CURRENCY={3}|RIC={4}",
        String.valueOf(timestamp),
        price == null? "": price,
        closePrice == null? "": closePrice,
        currency, ric);
  }
}
