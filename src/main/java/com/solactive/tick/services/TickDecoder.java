package com.solactive.tick.services;

import com.solactive.tick.model.Tick;
import com.solactive.tick.model.TickCurrency;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.math.BigDecimal;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class TickDecoder extends ByteToMessageDecoder {

  public static final String TICK_PROPERTY_DELIMITER = "\\|";
  public static final String TICK_KEY_VALUE_DELIMITER = "=";
  private final TickProcessingService tickProcessingService;

  protected void decode(ChannelHandlerContext channelHandlerContext,
                        ByteBuf byteBuf,
                        List<Object> list) {
    byte[] buffer = new byte[byteBuf.readableBytes()];
    byteBuf.readBytes(buffer);
    String tickStr = new String(buffer).trim();
    try {
      Tick tick = fromStringWithKeys(tickStr);
      log.debug("Received tick {}", tick);
      tickProcessingService.addTick(tick);
    } catch (Exception e) {
      log.warn("Invalid tick {}", tickStr);
    }
  }

  public Tick fromStringWithKeys(String tickStr) {
    String[] tickStrParts = tickStr.split(TICK_PROPERTY_DELIMITER);
    Long timestamp = Long.parseLong(tickStrParts[0].split(TICK_KEY_VALUE_DELIMITER)[1]);
    BigDecimal price = bigDecimalOrNull(tickStrParts[1].split(TICK_KEY_VALUE_DELIMITER));
    BigDecimal closePrice = bigDecimalOrNull(tickStrParts[2].split(TICK_KEY_VALUE_DELIMITER));
    TickCurrency
        tickCurrency = TickCurrency.valueOf(tickStrParts[3].split(TICK_KEY_VALUE_DELIMITER)[1]);
    String ric = tickStrParts[4].split(TICK_KEY_VALUE_DELIMITER)[1];
    Tick tick = new Tick(timestamp, price, closePrice, tickCurrency, ric);
    return tick;
  }

  private static BigDecimal bigDecimalOrNull(String[] keyValue) {
    return keyValue.length != 2 ? null : new BigDecimal(keyValue[1]);
  }

}
