package com.solactive.tick.services;

import com.solactive.tick.iterator.FileListBasedTickIterator;
import com.solactive.tick.model.Tick;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TicksExportService {
  private ExecutorService executorService;
  private final ConcurrentMap<String, AtomicInteger> fileCounters = new ConcurrentHashMap<>();
  @Value("${app.ticks.export.location:ticks}")
  public String baseDir;
  @Value("${app.ticks.export.pool.size:10}")
  private Integer poolSize;

  @PostConstruct
  public void init() {
    executorService = Executors.newFixedThreadPool(poolSize);
    File dir = new File(baseDir);
    dir.mkdirs();
    try {
      File[] directories = dir.listFiles();
      for (File directory : directories) {
        try {
          String ric = directory.getName();
          Integer max = Arrays.stream(Objects.requireNonNull(directory.listFiles()))
              .map(f -> Integer.parseInt(f.getName().split("-|\\.")[1]))
              .max(Integer::compare)
              .orElse(0);
          fileCounters.put(ric, new AtomicInteger(max + 1));
        } catch (Exception e) {
          log.warn("Couldn't initialize counter for directory {}", directory.getAbsolutePath(), e);
        }
      }
    } catch (Exception e) {
      log.error("Couldn't init counters", e);
    }
  }

  public Iterator<Tick> getRicArchiveIterator(String ric) {
    File dir = new File(baseDir + "/" + ric);
    List<File> files = Arrays.asList(dir.listFiles());
    files.sort(TickFileNameComparator.INSTANCE);
    return new FileListBasedTickIterator(files);
  }


  public void exportAsync(String ric, List<Tick> ticks) {
    executorService.submit(() -> {
      try {
        export(ric, ticks);
        log.info("Created tick csv for rick {}", ric);
      } catch (IOException e) {
        log.error("Couldn't create tick csv for ric {}", ric, e);
      }
    });
  }

  public void export(String ric, List<Tick> ticks) throws IOException {
    fileCounters.putIfAbsent(ric, new AtomicInteger());
    String ricDir =  baseDir + "/" + ric;
    File dir = new File(ricDir);
    dir.mkdirs();
    String filename = dir + "/" + System.currentTimeMillis() + "-" +
                      fileCounters.get(ric).getAndIncrement() + ".txt";
    File file = new File(filename);
    file.createNewFile();
    FileWriter out = new FileWriter(filename);
    try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)) {
      for (Tick tick : ticks) {
        printer.printRecord(tick.toRecord());
      }
    }
  }

  static class TickFileNameComparator implements Comparator<File> {
    static final TickFileNameComparator INSTANCE = new TickFileNameComparator();

    @Override
    public int compare(File o1, File o2) {
      String name1 = o1.getName();
      String name2 = o2.getName();
      String[] split1 = name1.split("-|\\.");
      String[] split2 = name2.split("-|\\.");
      Long timestamp1 = Long.parseLong(split1[0]);
      Long timestamp2 = Long.parseLong(split2[0]);
      Long num1 = Long.parseLong(split1[1]);
      Long num2 = Long.parseLong(split2[1]);
      int compare = Long.compare(timestamp1, timestamp2);
      return compare == 0 ? Long.compare(num1,num2): compare;
    }
  }

}
