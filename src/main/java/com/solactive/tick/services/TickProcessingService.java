package com.solactive.tick.services;

import com.solactive.tick.iterator.CompositeIterator;
import com.solactive.tick.model.Tick;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class TickProcessingService {
  private final ConcurrentMap<String, Object> locks = new ConcurrentHashMap<>();
  private final ConcurrentMap<String, LinkedList<Tick>> ticks = new ConcurrentHashMap<>();

  private final TicksExportService ticksExportService;

  public void addTick(Tick tick) {
    String ric = tick.getRic();
    this.ticks.putIfAbsent(ric, new LinkedList<>());
    this.locks.putIfAbsent(ric, new Object());
    Object lock = this.locks.get(ric);
    synchronized (lock) {
      LinkedList<Tick> ticks = this.ticks.get(ric);
      ticks.add(tick);
      if (tick.getClosePrice() != null) {
        this.ticks.put(ric, new LinkedList<>());
        ticksExportService.exportAsync(ric, ticks);
      }
    }
  }

  public Iterator<Tick> getAllRicTicksIterator(String ric) {
    Object o = locks.get(ric);
    if (o == null) {
      locks.putIfAbsent(ric, new Object());
      ticks.putIfAbsent(ric, new LinkedList<>());
      o = locks.get(ric);
    }
    synchronized (o) {
      ArrayList<Tick> ticks = new ArrayList<>(this.ticks.get(ric));
      Iterator<Tick> ricArchiveIterator = ticksExportService.getRicArchiveIterator(ric);
      return new CompositeIterator<>(List.of(ricArchiveIterator, ticks.iterator()));
    }
  }

}
