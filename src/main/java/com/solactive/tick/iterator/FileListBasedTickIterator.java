package com.solactive.tick.iterator;

import com.solactive.tick.model.Tick;
import com.solactive.tick.model.TickCurrency;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class FileListBasedTickIterator implements Iterator<Tick> {
  private final List<File> files;
  private int currentFileIndex = 0;
  private CSVParser parser;
  private Iterator<CSVRecord> iterator;

  public FileListBasedTickIterator(List<File> files) {
    this.files = files;
  }

  @Override
  public boolean hasNext() {
    if (iterator == null) {
      if (currentFileIndex == files.size()) {
        return false;
      }
      File currentFile = files.get(currentFileIndex);
      try {
        FileReader fr = new FileReader(currentFile);
        parser = new CSVParser(fr, CSVFormat.DEFAULT);
        iterator = parser.iterator();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
    try {
      if (!iterator.hasNext()) {
        while (!iterator.hasNext()) {
          parser.close();
          currentFileIndex++;
          if (currentFileIndex == files.size()) {
            return false;
          }
          File currentFile = files.get(currentFileIndex);
          FileReader fr = new FileReader(currentFile);
          parser = new CSVParser(fr, CSVFormat.DEFAULT);
          iterator = parser.iterator();
        }
      }
      return true;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Tick next() {
    try {
      if (!iterator.hasNext()) {
        while (!iterator.hasNext()) {
          parser.close();
          currentFileIndex++;
          if (currentFileIndex == files.size()) {
            throw new NoSuchElementException();
          }
          File currentFile = files.get(currentFileIndex);
          FileReader fr = new FileReader(currentFile);
          parser = new CSVParser(fr, CSVFormat.DEFAULT);
          iterator = parser.iterator();
        }
      }
      return getTick(iterator.next());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private Tick getTick(CSVRecord record) {
    long timestamp = Long.parseLong(record.get(0));
    BigDecimal price = record.get(1).isBlank() ? null : new BigDecimal(record.get(1));
    BigDecimal closePrice = record.get(2).isBlank() ? null : new BigDecimal(record.get(2));
    TickCurrency currency = TickCurrency.valueOf(record.get(3));
    String ric = record.get(4);
    return new Tick(timestamp, price, closePrice, currency, ric);
  }
}
