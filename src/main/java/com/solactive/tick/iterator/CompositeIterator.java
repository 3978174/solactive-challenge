package com.solactive.tick.iterator;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

@RequiredArgsConstructor
public class CompositeIterator<T> implements Iterator<T> {
  private final List<Iterator<T>> iterators;
  private int index = 0;
  private Iterator<T> current;

  @Override
  public boolean hasNext() {
    if (current == null) {
      if (iterators.size() == index) {
        return false;
      }
      current = iterators.get(index);
    }
    while(!current.hasNext()) {
      index++;
      if (iterators.size() == index) {
        return false;
      }
      current = iterators.get(index);
    }
    return true;
  }

  @Override
  public T next() {
    if (current == null) {
      if (iterators.size() == index) {
        throw new NoSuchElementException();
      }
      current = iterators.get(index);
    }
    while(!current.hasNext()) {
      index++;
      if (iterators.size() == index) {
        throw new NoSuchElementException();
      }
      current = iterators.get(index);
    }
    return current.next();
  }
}
