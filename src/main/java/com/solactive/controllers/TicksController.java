package com.solactive.controllers;

import com.solactive.controllers.dto.GetAllTicksResponse;
import com.solactive.tick.model.Tick;
import com.solactive.tick.services.TickProcessingService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class TicksController {
  private final TickProcessingService tickProcessingService;

  @GetMapping("/ticks")
  public GetAllTicksResponse getAllTicks(
      @RequestParam("ric") String ric,
      @RequestParam(value = "limit", defaultValue = "10") Integer limit,
      @RequestParam(value = "offset", defaultValue = "0") Integer offset) {
    log.info("Requested ticks for ric:{}, limit:{}, offset:{}", ric, limit, offset);
    Iterator<Tick> tickIterator = tickProcessingService.getAllRicTicksIterator(ric);
    List<Tick> responseTicks = new ArrayList<>(limit - offset);
    Integer currentPos = 0;
    while (tickIterator.hasNext() || currentPos.equals(limit)) {
      Tick tick = tickIterator.next();
      if (currentPos >= offset) {
        responseTicks.add(tick);
      }
      currentPos++;
    }
    return new GetAllTicksResponse(responseTicks);
  }
}
