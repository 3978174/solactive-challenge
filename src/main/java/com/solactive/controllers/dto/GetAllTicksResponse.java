package com.solactive.controllers.dto;

import com.solactive.tick.model.Tick;
import java.util.List;
import lombok.Data;

@Data
public class GetAllTicksResponse {
  private final List<Tick> ticks;
}
