package com.solactive.services;

import com.solactive.tick.model.Tick;
import com.solactive.tick.model.TickCurrency;
import com.solactive.tick.services.TicksExportService;
import com.solactive.tick.services.TickProcessingService;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
public class TickProcessingServiceTest {
  @MockBean
  private TicksExportService ticksExportService;
  @Captor
  private ArgumentCaptor<LinkedList<Tick>> captor;
  @Autowired
  private TickProcessingService tickProcessingService;

  @Test
  public void testAddTick() {
    Tick tick1 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, "ABC");
    Tick tick2 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, "BCD");
    Tick tick3 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, "ABC");
    Tick tick4 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, "ABC");
    Tick tick5 = new Tick(System.currentTimeMillis(), null, BigDecimal.ONE, TickCurrency.EUR, "BCD");
    tickProcessingService.addTick(tick1);
    tickProcessingService.addTick(tick2);
    tickProcessingService.addTick(tick3);
    tickProcessingService.addTick(tick4);
    tickProcessingService.addTick(tick5);

    LinkedList<Tick> expectedTicks = new LinkedList<>();
    expectedTicks.add(tick2);
    expectedTicks.add(tick5);
    Mockito.verify(ticksExportService, Mockito.times(1))
        .exportAsync(Mockito.eq("BCD"), Mockito.eq(expectedTicks));
  }

  @Test
  public void testAddTickMultiple() {
    Tick tick1 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, "ABC");
    Tick tick2 = new Tick(System.currentTimeMillis(), null, BigDecimal.ONE, TickCurrency.EUR, "ABC");
    Tick tick3 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, "ABC");
    Tick tick4 = new Tick(System.currentTimeMillis(),null, BigDecimal.ONE, TickCurrency.EUR, "ABC");
    Tick tick5 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, "ABC");
    Tick tick6 = new Tick(System.currentTimeMillis(), null, BigDecimal.ONE, TickCurrency.EUR, "ABC");
    tickProcessingService.addTick(tick1);
    tickProcessingService.addTick(tick2);
    tickProcessingService.addTick(tick3);
    tickProcessingService.addTick(tick4);
    tickProcessingService.addTick(tick5);
    tickProcessingService.addTick(tick6);

    LinkedList<Tick> expectedTicks1 = new LinkedList<>();
    expectedTicks1.add(tick1);
    expectedTicks1.add(tick2);
    LinkedList<Tick> expectedTicks2 = new LinkedList<>();
    expectedTicks2.add(tick3);
    expectedTicks2.add(tick4);
    LinkedList<Tick> expectedTicks3 = new LinkedList<>();
    expectedTicks3.add(tick5);
    expectedTicks3.add(tick6);
    Mockito.verify(ticksExportService, Mockito.times(3))
        .exportAsync(Mockito.eq("ABC"), captor.capture());
    Assertions.assertEquals(expectedTicks1, captor.getAllValues().get(0));
    Assertions.assertEquals(expectedTicks2, captor.getAllValues().get(1));
    Assertions.assertEquals(expectedTicks3, captor.getAllValues().get(2));
  }

  @Test
  public void testGetAllRicTicksIterator() {
    String ric = "rrr";
    Tick tick1 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, ric);
    Tick tick2 = new Tick(System.currentTimeMillis(), null, BigDecimal.ONE, TickCurrency.EUR, ric);
    List<Tick> ticks = List.of(tick1, tick2);
    Tick tick3 = new Tick(System.currentTimeMillis(), BigDecimal.TEN, null, TickCurrency.EUR, ric);
    Tick tick4 = new Tick(System.currentTimeMillis(), BigDecimal.ZERO, null, TickCurrency.EUR, ric);
    Mockito.when(ticksExportService.getRicArchiveIterator(Mockito.eq(ric))).thenReturn(ticks.iterator());

    tickProcessingService.addTick(tick3);
    tickProcessingService.addTick(tick4);

    Iterator<Tick> iterator = tickProcessingService.getAllRicTicksIterator(ric);

    Assertions.assertEquals(tick1, iterator.next());
    Assertions.assertEquals(tick2, iterator.next());
    Assertions.assertEquals(tick3, iterator.next());
    Assertions.assertEquals(tick4, iterator.next());
  }
}
