package com.solactive;

import com.solactive.tick.services.TickDecoder;
import com.solactive.tick.services.TickProcessingService;
import com.solactive.tick.model.Tick;
import com.solactive.tick.model.TickCurrency;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import java.math.BigDecimal;
import java.text.MessageFormat;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
public class TickDecoderTest {
  @MockBean
  private TickProcessingService tickProcessingService;
  @Autowired
  private TickDecoder tickDecoder;

  @Test
  public void testDecode() {
    Tick tick = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, "RIC");
    String tickStr = toString(tick);

    EmbeddedChannel channel = new EmbeddedChannel(tickDecoder);
    channel.writeInbound(Unpooled.wrappedBuffer(tickStr.getBytes()));
    Mockito.verify(tickProcessingService, Mockito.times(1)).addTick(Mockito.eq(tick));
  }

  @Test
  public void testDecodeInvalid() {
    String brokenTickStr = "brokenStringHere";

    EmbeddedChannel channel = new EmbeddedChannel(tickDecoder);
    channel.writeInbound(Unpooled.wrappedBuffer(brokenTickStr.getBytes()));
    Mockito.verify(tickProcessingService, Mockito.never()).addTick(Mockito.any());
  }

  private String toString(Tick tick) {
    return MessageFormat.format(
        "TIMESTAMP={0}|PRICE={1}|CLOSE_PRICE={2}|CURRENCY={3}|RIC={4}",
        String.valueOf(tick.getTimestamp()),
        tick.getPrice() == null? "": tick.getPrice(),
        tick.getClosePrice() == null? "": tick.getClosePrice(),
        tick.getCurrency(), tick.getRic());
  }
}


