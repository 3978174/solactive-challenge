package com.solactive.controllers;

import com.solactive.controllers.dto.GetAllTicksResponse;
import com.solactive.tick.model.Tick;
import com.solactive.tick.model.TickCurrency;
import com.solactive.tick.services.TicksExportService;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class TicksControllerTest {

  @Autowired
  private TicksController ticksController;
  @MockBean
  private TicksExportService ticksExportService;


  @Test
  public void testGetAllTicks() {
    String ric = "rrr";

    Tick tick1 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, ric);
    Tick tick2 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, ric);
    Tick tick3 = new Tick(System.currentTimeMillis(), BigDecimal.ONE, null, TickCurrency.EUR, ric);
    List<Tick> ticks = List.of(tick1, tick2, tick3);
    Mockito.when(ticksExportService.getRicArchiveIterator(Mockito.eq(ric))).thenReturn(ticks.iterator());
    GetAllTicksResponse allTicks = ticksController.getAllTicks(ric, 10, 0);
    Assertions.assertEquals(ticks.size(), allTicks.getTicks().size());
    for (int i = 0; i < ticks.size(); i++) {
      Assertions.assertEquals(ticks.get(i), allTicks.getTicks().get(i));
    }
  }

}
